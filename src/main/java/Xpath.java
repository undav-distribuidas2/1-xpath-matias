import java.io.IOException;
import java.util.List;

import org.jdom2.*;

import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

public class Xpath {

    Document document = null;

    public Xpath(){
        SAXBuilder sax = new SAXBuilder();

        try {
            this.document = sax.build("/home/matias/workspace-java/employees.xml");
        }
        catch (JDOMException e) {
            e.printStackTrace();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String createQuery(String query){
        XPathFactory xpfac = XPathFactory.instance();
        XPathExpression<Content> expr = xpfac.compile(query, Filters.content());
        List<Content> links = expr.evaluate(document);

        String outputXML = "";
        outputXML = new XMLOutputter().outputString(links);

        return outputXML;
    }

}

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class Table {
    private JButton JButtonSearch;
    private JTextField textFieldSearch;
    private JTextArea textArea1;
    public JPanel JPanelMain;
    private javax.swing.JScrollPane JScrollPane;
    private Xpath miXpath;


    public Xpath getMiXpath() {
        return miXpath;
    }
    public void setMiXpath(Xpath miXpath) {
        this.miXpath = miXpath;
    }

    public Table(Xpath miXpath){

        createFrame();
        JButtonSearch.addActionListener(new JButtonSearchClicked());
        setMiXpath(miXpath);

        this.JScrollPane.setViewportView(textArea1);

    }

    private class JButtonSearchClicked implements ActionListener {
        @Override

        public void actionPerformed(ActionEvent e) {
            textArea1.setText(getMiXpath().createQuery(textFieldSearch.getText()));
            System.out.println(getMiXpath().createQuery(textFieldSearch.getText()));
        }
    }

    private void createFrame(){
        JFrame frame = new JFrame("XPath");
        frame.setContentPane(this.JPanelMain);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

    }

}
